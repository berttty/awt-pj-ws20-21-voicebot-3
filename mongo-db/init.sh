#!/bin/bash

# validate that the docker is installed
if ! command -v docker &> /dev/null
then
  echo "follow the instruction in https://docs.docker.com/desktop/mac/install/"
  exit 1
fi

#install minikube if not installed
if ! command -v minikube &> /dev/null
then
  curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64
  sudo install minikube-darwin-amd64 /usr/local/bin/minikube
fi

#create the cluster of kubernetes
minikube start -p webtech

#apply the configuration required for mongodb
kubectl apply -f mongodb-secrets.yaml
kubectl apply -f mongodb-pv.yaml
kubectl apply -f mongodb-pvc.yaml
kubectl apply -f mongodb-nodeport-svc.yaml
kubectl apply -f mongodb-deployment.yaml

#create the link between host of docker and mongodb
sudo docker run --rm -it --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:28000,reuseaddr,fork TCP:$(minikube ip -p webtech):32000" &

#install mongoimport if not installed
if ! command -v mongoimport &> /dev/null
then
  brew install mongodb/brew/mongodb-database-tools
fi

#sleep 5 minutes to wait the download of images
sleep 300

#get the container name
NAME_CONTAINER=$(kubectl get pods | grep 'mongo' | tr ' ' '\n' | head -n 1)

#Copy the files to the container
kubectl cp user.json ${NAME_CONTAINER}:/user.json
kubectl cp doctype.json ${NAME_CONTAINER}:/doctype.json
kubectl cp userdoctype.json ${NAME_CONTAINER}:/userdoctype.json


#create the data base and add the data to mongodb
kubectl exec --stdin --tty ${NAME_CONTAINER} -- /bin/mongoimport --username adminuser --password password123 mongodb://localhost:27017 --db=admin --collection=user --file=user.json
kubectl exec --stdin --tty ${NAME_CONTAINER} -- /bin/mongoimport --username adminuser --password password123 mongodb://localhost:27017 --db=admin --collection=user --file=doctype.json
kubectl exec --stdin --tty ${NAME_CONTAINER} -- /bin/mongoimport --username adminuser --password password123 mongodb://localhost:27017 --db=admin --collection=user --file=userdoctype.json

#Open the ports locally
kubectl port-forward deployment/mongo 27017:27017