package org.tuberlin.webtech.voicebotbackend.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import org.tuberlin.webtech.voicebotbackend.entity.UserDocType;
import org.tuberlin.webtech.voicebotbackend.service.SequenceGeneratorService;

@Component
public class UserDocTypeModelListener extends AbstractMongoEventListener<UserDocType> {

    private SequenceGeneratorService sequenceGenerator;

    @Autowired
    public UserDocTypeModelListener(SequenceGeneratorService sequenceGenerator) {
        this.sequenceGenerator = sequenceGenerator;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<UserDocType> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId(sequenceGenerator.generateSequence(UserDocType.SEQUENCE_NAME));
        }
    }

}