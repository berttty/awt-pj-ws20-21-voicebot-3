package org.tuberlin.webtech.voicebotbackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.tuberlin.webtech.voicebotbackend.entity.UserDocType;

import java.util.List;

public interface UserDocTypeRepository extends MongoRepository<UserDocType, Long> {

    List<UserDocType> findByUserstatus(@Param("userstatus") String user_status);

    List<UserDocType> findByUsertype(@Param("usertype") String user_type);
}
