package org.tuberlin.webtech.voicebotbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class VoicebotBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoicebotBackendApplication.class, args);
	}

}
