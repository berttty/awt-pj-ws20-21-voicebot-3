package org.tuberlin.webtech.voicebotbackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.tuberlin.webtech.voicebotbackend.entity.User;

import java.util.List;

public interface UserRepository extends MongoRepository<User, Long> {

    List<User> findByName(@Param("name") String name);
}
