package org.tuberlin.webtech.voicebotbackend.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;
import org.tuberlin.webtech.voicebotbackend.entity.DocType;
import org.tuberlin.webtech.voicebotbackend.service.SequenceGeneratorService;

@Component
public class DocTypeModelListener extends AbstractMongoEventListener<DocType> {

    private SequenceGeneratorService sequenceGenerator;

    @Autowired
    public DocTypeModelListener(SequenceGeneratorService sequenceGenerator) {
        this.sequenceGenerator = sequenceGenerator;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<DocType> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId(sequenceGenerator.generateSequence(DocType.SEQUENCE_NAME));
        }
    }

}