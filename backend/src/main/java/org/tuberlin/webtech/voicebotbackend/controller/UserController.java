package org.tuberlin.webtech.voicebotbackend.controller;

import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.tuberlin.webtech.voicebotbackend.entity.DocType;
import org.tuberlin.webtech.voicebotbackend.entity.User;
import org.tuberlin.webtech.voicebotbackend.entity.UserDocType;
import org.tuberlin.webtech.voicebotbackend.repository.DocTypeRepository;
import org.tuberlin.webtech.voicebotbackend.repository.UserDocTypeRepository;
import org.tuberlin.webtech.voicebotbackend.repository.UserRepository;
import org.tuberlin.webtech.voicebotbackend.service.SequenceGeneratorService;


import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DocTypeRepository docTypesRepository;

    @Autowired
    private UserDocTypeRepository userDocTypesRepository;

    @Autowired
    private SequenceGeneratorService sequenceGenerator;

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    @ResponseBody
    public String addUser(@ModelAttribute User user) {
        user.setId(sequenceGenerator.generateSequence(User.SEQUENCE_NAME));

        try {
            userRepository.save(user);
        }catch (Exception e){
            return "User " + user.getName() + " Already exists";
        }
        return "User added successfully::"+user.getId();
    }

    @RequestMapping(value = "/user/get", method = RequestMethod.GET)
    public User getUser(
            @RequestParam("name") String name
    ){
        // User.name is defined as unique in DB
        List<User> usrs = userRepository.findByName(name);
        if(usrs.size() == 1)
            return usrs.get(0);
        else
            return null;
    }

    @RequestMapping(value = "/doctype/add", method = RequestMethod.POST)
    @ResponseBody
    public String addDocType(@ModelAttribute DocType dt) {
        dt.setId(sequenceGenerator.generateSequence(DocType.SEQUENCE_NAME));
        docTypesRepository.save(dt);
        return "Doc Type added successfully::"+dt.getId();
    }

    @RequestMapping(value = "/doctype/get", method = RequestMethod.GET)
    public DocType getDocType(
            @RequestParam("name") String name
    ){
        // DocType.name is defined as unique in DB
        List<DocType> docs = docTypesRepository.findByName(name);
        if(docs.size() == 1)
            return docs.get(0);
        else
            return null;
    }

    @RequestMapping(value = "/userdoc/add/old", method = RequestMethod.POST)
    @ResponseBody
    public String addUserDocTypeOld(@ModelAttribute UserDocType udt) {
        udt.setId(sequenceGenerator.generateSequence(DocType.SEQUENCE_NAME));
        userDocTypesRepository.save(udt);
        return "User Doc Type added successfully::"+udt.getId();
    }

    @RequestMapping(value = "/userdoc/add", method = RequestMethod.POST)
    @ResponseBody
    public String addUserDocType(
            @RequestParam("user") String user,
            @RequestParam("type") String type,
            @RequestParam("status") String status
    ) {
        UserDocType udt = new UserDocType();

        udt.setId(sequenceGenerator.generateSequence(UserDocType.SEQUENCE_NAME));
        udt.setUserstatus(user+"|"+status);
        udt.setUsertype(user+"|"+type);

        switch (status){
            case "pending":
                udt.setDetails("The user have not submitted any attempt yet");
                break;
            case "rejected":
                try {
                    switch (type) {
//                    Transcript of record        records
//                    Language proficiency        language
//                    Insurance certificate       insurance
//                    Police clearance            police
//                    Bachelor certificate        bachelor
                        case "police":
                            udt.setDetails("There are criminal records that must be reviewed by a human, please contact your target institution");
                            break;
                        case "bachelor":
                            udt.setDetails("Your bachelor certificate was not signed by the pertinent authorities of your country, please upload it after validation");
                            break;
                        case "insurance":
                            udt.setDetails("Your insurance certificate does not accomplish the minimum requirements defined by the German health department, please improve the characteristics of your plan");
                            break;
                        case "language":
                            udt.setDetails("Your Language proficiency is below the minimum requirements defined by your target institution, it must be at least B2");
                            break;
                        case "records":
                            udt.setDetails("The submitted document is corrupted, please review your file and submit it correctly");
                            break;
                    }
                    break;
                } catch (Exception e) {
                    return "Type of document already exists";
                }
            case "approved":
                udt.setDetails("The submitted document has been approved successfully");
                break;
            default:
                return "Status type not supported";
        }

        try {
            userDocTypesRepository.save(udt);
        } catch (Exception e){
            return "User already has submitted a document of this type";
        }

        return "User Doc Type added successfully::"+udt.getId();
    }

    @RequestMapping(value = "/userdoc/get", method = RequestMethod.GET)
    public List<UserDocType> getUserDocType(
            @RequestParam("user") String user,
            @RequestParam("status") String status
    ){
        // DocType.name is defined as unique in DB
        List<UserDocType> docs = userDocTypesRepository.findByUserstatus(user+"|"+status);
        return docs;
    }

    @RequestMapping(value = "/userdoc/type/get", method = RequestMethod.GET)
    public UserDocType getUserDocTypeByType(
            @RequestParam("user") String user,
            @RequestParam("type") String type
    ) throws Exception {
        // DocType.name is defined as unique in DB
        List<UserDocType> docs = userDocTypesRepository.findByUsertype(user+"|"+type);

        if(docs.size()>1){
            throw new Exception("Inconsistent number of documents " + type + " attached to the user " + user);
        } else if (docs.size() == 0){
            return null;
        } else{
            return docs.get(0);
        }

    }

    @GetMapping("/hi")
    public String saveProduct() {
        return "Hola Rodrigo, recuérdala con amor, no con tristeza.";
    }


    /**Entrypoints for voicebots */

    @RequestMapping(value = "/user/auth", method = RequestMethod.POST)
    @ResponseBody
    public Boolean authUser(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) {
        User us = getUser(user);
        if (us == null)
            return false;
        return us.getPass().equals(pass);
    }

    @RequestMapping(value = "/user/app/old", method = RequestMethod.GET)
    @ResponseBody
    public Boolean appUser(
            @RequestParam("user") String user
    ) {
        User us = getUser(user);
        if (us == null)
            return false;
        return us.isApp();
    }

    @RequestMapping(value = "/userdoc/missing/old", method = RequestMethod.POST)
    @ResponseBody
    public String gerUserMissing(
            @RequestParam("user") String user
    ) throws Exception {
        List<UserDocType> pending = getUserDocType(user, "pending");
        String res = "";
        if (pending.size() > 0 ) {
            for (UserDocType t : pending) {
                res += mapDocType(t.getUsertype().split("|")[1]) + ": " + t.getDetails() + ". \\n";
            }
        }

        return res;
    }

    @RequestMapping(value = "/userdoc/approved/old", method = RequestMethod.POST)
    @ResponseBody
    public String gerUserApproved(
            @RequestParam("user") String user
    ) throws Exception {
        List<UserDocType> approved = getUserDocType(user, "approved");
        String res = "";
        if (approved.size() > 0 ) {
            for (UserDocType t : approved) {
                res += mapDocType(t.getUsertype().split("|")[1]) + ": " + t.getDetails() + ". \\n";
            }
        }

        return res;
    }

    @RequestMapping(value = "/userdoc/rejected/old", method = RequestMethod.POST)
    @ResponseBody
    public String gerUserRejected(
            @RequestParam("user") String user
    ) throws Exception {
        List<UserDocType> rejected = getUserDocType(user, "rejected");
        String res = "";
        if (rejected.size() > 0 ) {
            for (UserDocType t : rejected) {
                res += mapDocType(t.getUsertype().split("|")[1]) + ": " + t.getDetails() + ". \\n";
            }
        }

        return res;
    }

    @RequestMapping(value = "/userdoc/submitted/old", method = RequestMethod.POST)
    @ResponseBody
    public String gerUserSubmitted(
            @RequestParam("user") String user
    ) throws Exception {
        List<UserDocType> rejected = getUserDocType(user, "rejected");
        List<UserDocType> approved = getUserDocType(user, "approved");
        String res = "";
        if (rejected.size() == 0 && approved.size() == 0) {
            return "No documents have been submitted by the user";
        }

        if (rejected.size() > 0 ) {
            for (UserDocType t : rejected) {
                res += mapDocType(t.getUsertype().split("|")[1]) + ": " + t.getDetails() + ". \\n";
            }
        }

        if (approved.size() > 0) {
            for (UserDocType t : approved) {
                res += mapDocType(t.getUsertype().split("|")[1]) + ": " + t.getDetails() + ". \\n";
            }
        }

        return res;
    }

    @RequestMapping(value = "/time/remaining/old", method = RequestMethod.GET)
    @ResponseBody
    public String timeRemaining() {

        Date now = Date.from(Instant.now());

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            Date deadline = sdf.parse("05/31/2022");


            long diffInMillies = Math.abs(deadline.getTime() - now.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            return "You  still have " + diff + " days to complete your application";
        } catch (Exception e){

            return "There is something wrong, I cannot provide you that information at this moment";
        }

    }

    public String mapDocType(String code) throws Exception {

        switch (code) {
            case "records":
                return "Transcript of record";
            case "language":
                return "Language proficiency";
            case "insurance":
                return "Insurance certificate";
            case "police":
                return  "Police clearance";
            case "bachelor":
                return "Bachelor certificate";
            default:
                throw new Exception("Document type not supported: " + code);
        }

    }

    /**
     * {
     *    "detectIntentResponseId":"1eec2d17-fe53-4505-a796-d6b088094a72",
     *    "intentInfo":{
     *       "lastMatchedIntent":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/intents/2aaf4b74-9b6e-4015-b745-51a26b9d3cde",
     *       "displayName":"test",
     *       "confidence":1.0
     *    },
     *    "pageInfo":{
     *       "currentPage":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/flows/00000000-0000-0000-0000-000000000000/pages/START_PAGE",
     *       "displayName":"Start Page"
     *    },
     *    "sessionInfo":{
     *       "session":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/sessions/5d536c-ebf-f00-b82-1b1af88c5"
     *    },
     *    "fulfillmentInfo":{
     *       "tag":"lala"
     *    },
     *    "text":"test",
     *    "languageCode":"en"
     * }
     * */

    /**
     * {
     *    "detectIntentResponseId":"5ef499b3-3ed7-47d0-9b78-8edb89be82a2",
     *    "intentInfo":{
     *       "lastMatchedIntent":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/intents/2aaf4b74-9b6e-4015-b745-51a26b9d3cde",
     *       "displayName":"test",
     *       "confidence":1.0
     *    },
     *    "pageInfo":{
     *       "currentPage":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/flows/00000000-0000-0000-0000-000000000000/pages/START_PAGE",
     *       "displayName":"Start Page"
     *    },
     *    "sessionInfo":{
     *       "session":"projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/sessions/5d536c-ebf-f00-b82-1b1af88c5",
     *       "parameters":{
     *          "pass":123.0,
     *          "person":{
     *             "name":"claire",
     *             "original":"claire"
     *          },
     *          "given-name": "Alan"
     *       }
     *    },
     *    "fulfillmentInfo":{
     *       "tag":"lala"
     *    },
     *    "text":"test",
     *    "languageCode":"en"
     * }
     * */

    public static void main(String[] args) {
        //messageParser("{'detectIntentResponseId': '5ef499b3-3ed7-47d0-9b78-8edb89be82a2', 'intentInfo': {'lastMatchedIntent': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/intents/2aaf4b74-9b6e-4015-b745-51a26b9d3cde', 'displayName': 'test', 'confidence': 1.0}, 'pageInfo': {'currentPage': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/flows/00000000-0000-0000-0000-000000000000/pages/START_PAGE', 'displayName': 'Start Page'}, 'sessionInfo': {'session': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/sessions/5d536c-ebf-f00-b82-1b1af88c5', 'parameters': {'pass': 123.0, 'given-name': 'claire'}}, 'fulfillmentInfo': {'tag': 'lala'}, 'text': 'test', 'languageCode': 'en'}");
        Map<String, String> params = messageParser("{'detectIntentResponseId': '9261ca22-98c6-4b81-981c-a578ffc0dd93', 'intentInfo': {'lastMatchedIntent': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/intents/445fd87a-5309-4206-8e86-81344686ec63', 'parameters': {'pass': {'originalValue': '123', 'resolvedValue': 123.0}}, 'displayName': 'user.password', 'confidence': 1.0}, 'pageInfo': {'currentPage': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/flows/86576b08-da30-4993-8a22-97859b710247/pages/e7ee45f8-16f7-46cf-a566-1884a61953cd', 'formInfo': {'parameterInfo': [{'displayName': 'pass', 'required': True, 'state': 'FILLED', 'value': 123.0}]}, 'displayName': 'Status Provision'}, 'sessionInfo': {'session': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/sessions/d78b31-ce9-880-8ac-e2ed53b01', 'parameters': {'given-name': 'Claire', 'pass': 123.0}}, 'fulfillmentInfo': {'tag': 'app_status'}, 'messages': [{'text': {'text': ['Checking your credentials...'], 'redactedText': ['Checking your credentials...']}, 'responseType': 'ENTRY_PROMPT', 'source': 'VIRTUAL_AGENT'}, {'text': {'text': ['Your Application has problems with 2 documents\\nBut you still have 1 Month and 12 days to fix them'], 'redactedText': ['Your Application has problems with 2 documents\\nBut you still have 1 Month and 12 days to fix them']}, 'responseType': 'HANDLER_PROMPT', 'source': 'VIRTUAL_AGENT'}], 'text': '123', 'languageCode': 'en'}");

        System.out.println(params);
    }


    /**
     * {'detectIntentResponseId': '9261ca22-98c6-4b81-981c-a578ffc0dd93', 'intentInfo': {'lastMatchedIntent': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/intents/445fd87a-5309-4206-8e86-81344686ec63', 'parameters': {'pass': {'originalValue': '123', 'resolvedValue': 123.0}}, 'displayName': 'user.password', 'confidence': 1.0}, 'pageInfo': {'currentPage': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/flows/86576b08-da30-4993-8a22-97859b710247/pages/e7ee45f8-16f7-46cf-a566-1884a61953cd', 'formInfo': {'parameterInfo': [{'displayName': 'pass', 'required': True, 'state': 'FILLED', 'value': 123.0}]}, 'displayName': 'Status Provision'}, 'sessionInfo': {'session': 'projects/lala-335616/locations/us-west1/agents/2758b65f-5444-4964-863a-ac7960379951/sessions/d78b31-ce9-880-8ac-e2ed53b01', 'parameters': {'given-name': 'Claire', 'pass': 123.0}}, 'fulfillmentInfo': {'tag': 'app_status'}, 'messages': [{'text': {'text': ['Checking your credentials...'], 'redactedText': ['Checking your credentials...']}, 'responseType': 'ENTRY_PROMPT', 'source': 'VIRTUAL_AGENT'}, {'text': {'text': ['Your Application has problems with 2 documents\nBut you still have 1 Month and 12 days to fix them'], 'redactedText': ['Your Application has problems with 2 documents\nBut you still have 1 Month and 12 days to fix them']}, 'responseType': 'HANDLER_PROMPT', 'source': 'VIRTUAL_AGENT'}], 'text': '123', 'languageCode': 'en'}
     * */
    public static Map<String, String> messageParser(String request) {

       // Gson gson = new Gson();
        JsonObject job = new JsonParser().parse(request).getAsJsonObject();

        //JsonObject job = gson.fromJson(request, JsonObject.class);
//        String str =
//                job.getAsJsonObject("queryResult")
//                        .getAsJsonObject("intent")
//                        .getAsJsonPrimitive("displayName")
//                        .toString();

        //JsonObject intentInfo = job.getAsJsonObject("intentInfo");
        //JsonObject pageInfo = job.getAsJsonObject("pageInfo");
        JsonObject sessionInfo = job.getAsJsonObject("sessionInfo");
        // Might not be there
        //JsonObject parameters = sessionInfo.getAsJsonObject("parameters");
        //JsonObject fulfillmentInfo = job.getAsJsonObject("fulfillmentInfo");

        Set<Map.Entry<String, JsonElement>> params;
        if(sessionInfo.has("parameters")) {
            params = sessionInfo.getAsJsonObject("parameters").entrySet();
        } else {
            params = new HashSet<>();
        }

        //String fullTag = fulfillmentInfo.getAsJsonPrimitive ("tag").getAsString();
        //String input = job.getAsJsonPrimitive("text").getAsString();

        Map<String, String> mappedParams = new HashMap<String, String>();
        for(Map.Entry<String, JsonElement> entry : params)
        {
            System.out.println("Key: " + entry.getKey());
            System.out.println("Value: " + entry.getValue());

            if(entry.getValue().isJsonNull())
                mappedParams.put(entry.getKey(), "");
            else
                mappedParams.put(entry.getKey(), entry.getValue().getAsString());

        }

        //System.out.println("job");
        //System.out.println(intentInfo);
        if(mappedParams.containsKey("given-name"))
            System.out.println(mappedParams.get("given-name"));

        //System.out.println(job.get("sessionInfo").getAsJsonObject());
        //mappedParams.put("fulfillment", input);
        //mappedParams.put("webhook_tag", fullTag);
        return mappedParams;
//
//        JsonObject o = null;
//        String a = '"' + "Default Welcome Intent" + '"';
//        String b = '"' + "get-agent-name" + '"';
//        String responseText = "";
//
//        if (str.equals(a)) {
//            responseText = '"' + "Hello from a Java GCF Webhook" + '"';
//        } else if (str.equals(b)) {
//            responseText = '"' + "My name is Flowhook" + '"';
//        } else {
//            responseText = '"' + "Sorry I didn't get that" + '"';
//        }
//
//        o =
//                parser
//                        .parse(
//                                "{\"fulfillmentMessages\": [ { \"text\": { \"text\": [ "
//                                        + responseText
//                                        + " ] } } ] }")
//                        .getAsJsonObject();

    }

    @RequestMapping(value = "/user/app/older", method = RequestMethod.POST)
    @ResponseBody
    public String voiceAppUserOlder(
            @RequestBody String json
    ) {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);
        String webhookresp;

        if (us == null)
            webhookresp = "Authentication failed, give me your password again please";
        else if(us.isApp())
            webhookresp = "Congratulations " + us.getName() + ". Your application has been approved";
        else
            webhookresp = "Your Application has problems, Do you want to check the documents?";

        return buildAnswer(webhookresp);
    }

    @RequestMapping(value = "/userdoc/submitted", method = RequestMethod.POST)
    @ResponseBody
    public String voiceUserSubmitted(
            @RequestBody String json
            //@RequestParam("user") String user
    ) throws Exception {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        List<UserDocType> rejected = getUserDocType(us.getName(), "rejected");
        List<UserDocType> approved = getUserDocType(us.getName(), "approved");
        String res = "";

        if (rejected.size() == 0 && approved.size() == 0) {
            return "No documents have been submitted by you " + us.getName();
        }

        if (rejected.size() > 0 ) {
            for (UserDocType t : rejected) {
                res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
            }
        }

        if (approved.size() > 0) {
            for (UserDocType t : approved) {
                res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
            }
        }

        return buildAnswer(res);
    }


    @RequestMapping(value = "/userdoc/rejected", method = RequestMethod.POST)
    @ResponseBody
    public String voiceUserRejected(
            @RequestBody String json
    ) throws Exception {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        List<UserDocType> rejected = getUserDocType(us.getName(), "rejected");

        if (rejected.size() == 0) {
            return buildAnswer("No documents have been rejected for you " + us.getName());
        }

        String res = "";
        for (UserDocType t : rejected) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return buildAnswer(res);
    }

    @RequestMapping(value = "/userdoc/approved", method = RequestMethod.POST)
    @ResponseBody
    public String voiceUserApproved(
            @RequestBody String json
    ) throws Exception {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        List<UserDocType> approved = getUserDocType(us.getName(), "approved");

        if (approved.size() == 0) {
            return buildAnswer("No documents have been approved for you " + us.getName());
        }

        String res = "";
        for (UserDocType t : approved) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return buildAnswer(res);
    }

    @RequestMapping(value = "/userdoc/missing", method = RequestMethod.POST)
    @ResponseBody
    public String voiceUserMissing(
            @RequestBody String json
    ) throws Exception {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        List<UserDocType> pending = getUserDocType(us.getName(), "pending");

        if (pending.size() == 0) {
            return buildAnswer("You do not have any pending documents");
        }

        String res = "";
        for (UserDocType t : pending) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return buildAnswer(res);
    }

    @RequestMapping(value = "/time/remaining", method = RequestMethod.POST)
    @ResponseBody
    public String voiceTimeRemaining(
            @RequestBody String json
    ) {

        Date now = Date.from(Instant.now());

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            Date deadline = sdf.parse("05/31/2022");


            long diffInMillies = Math.abs(deadline.getTime() - now.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            return buildAnswerWithPayload("You  still have " + diff + " days to complete your application");
        } catch (Exception e){

            return buildAnswerWithPayload("There is something wrong, I cannot provide you that information at this moment");
        }

    }

    @RequestMapping(value = "/user/app", method = RequestMethod.POST)
    @ResponseBody
    public String VoiceAppUser(
            @RequestBody String json
    ) {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        if(us.isApp()) {
            return buildAnswer("Congratulations, your application has been already approved");
        } else {

            return buildAnswer("There are still documents that are missing or rejected");
        }
    }

    @RequestMapping(value = "/userdoc/status", method = RequestMethod.POST)
    @ResponseBody
    public String VoiceUserDocStatus(
            @RequestBody String json
    ) {
        Map<String, String> params = messageParser(json);
        User us = authUser(params);

        if (us == null) {
            return buildAnswer("Authentication failed, give me your password again please");
        }

        try {
            UserDocType document = getUserDocTypeByType(us.getName(), params.get("document"));

            if(document == null) {
                return buildAnswer("No document of that type has been submitted");
            } else {

                return buildAnswer("Your " + mapDocType(document.getUsertype().split("\\|")[1]) + " has been " + document.getUserstatus().split("\\|")[1] + " with the following feedback: " + document.getDetails());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Unable to find any document of that type for you " + us.getName();
    }

    public User authUser(Map<String, String> params)
    {
        User us = getUser(params.get("given-name").toLowerCase(Locale.ROOT));
        float passf = Float.parseFloat(params.get("pass"));
        int pass = Math.round(passf);

        if (us == null)
            return null;
        if(!us.getPass().equals(pass+""))
            return null;
        else
            return us;
    }

    public String buildAnswer(String resp){

        return "{\"fulfillment_response\": {\"messages\": [{\"text\": {\"text\": [\"" + resp + "\"]}}],\"merge_behavior\": \"REPLACE\"}}";

    }

    public String buildAnswerWithPayload(String resp/*, Map<String, String> payloads*/){

        return "{" +
                "\"fulfillment_response\": {\"messages\": [{\"text\": {\"text\": [\"" + resp + "\"]}}],\"merge_behavior\": \"REPLACE\"}" +
                ",\"richContent\": [[{\"lala\": \"lalo\"}]]" +
                "}";

    }


    /**
     * Regular entrypoints to be used
     * */

    @RequestMapping(value = "/gen/userdoc/submitted", method = RequestMethod.POST)
    @ResponseBody
    public String generalUserSubmitted(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) throws Exception {

        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        List<UserDocType> rejected = getUserDocType(us.getName(), "rejected");
        List<UserDocType> approved = getUserDocType(us.getName(), "approved");
        String res = "";

        if (rejected.size() == 0 && approved.size() == 0) {
            return "No documents have been submitted by you " + us.getName();
        }

        if (rejected.size() > 0 ) {
            for (UserDocType t : rejected) {
                res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
            }
        }

        if (approved.size() > 0) {
            for (UserDocType t : approved) {
                res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
            }
        }

        return res;
    }


    @RequestMapping(value = "/gen/userdoc/rejected", method = RequestMethod.POST)
    @ResponseBody
    public String generalUserRejected(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) throws Exception {

        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        List<UserDocType> rejected = getUserDocType(us.getName(), "rejected");

        if (rejected.size() == 0) {
            return "No documents have been rejected for you " + us.getName();
        }

        String res = "";
        for (UserDocType t : rejected) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return res;
    }

    @RequestMapping(value = "/gen/userdoc/approved", method = RequestMethod.POST)
    @ResponseBody
    public String generalUserApproved(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) throws Exception {

        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        List<UserDocType> approved = getUserDocType(us.getName(), "approved");

        if (approved.size() == 0) {
            return "No documents have been approved for you " + us.getName();
        }

        String res = "";
        for (UserDocType t : approved) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return res;
    }

    @RequestMapping(value = "/gen/userdoc/missing", method = RequestMethod.POST)
    @ResponseBody
    public String generalUserMissing(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) throws Exception {

        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        List<UserDocType> pending = getUserDocType(us.getName(), "pending");

        if (pending.size() == 0) {
            return "You do not have any pending documents";
        }

        String res = "";
        for (UserDocType t : pending) {
            res += mapDocType(t.getUsertype().split("\\|")[1]) + ": " + t.getDetails() + ". ";
        }

        return res;
    }

    @RequestMapping(value = "/gen/time/remaining", method = RequestMethod.POST)
    @ResponseBody
    public String generalTimeRemaining() {

        Date now = Date.from(Instant.now());

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            Date deadline = sdf.parse("05/31/2022");


            long diffInMillies = Math.abs(deadline.getTime() - now.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            return "You  still have " + diff + " days to complete your application";
        } catch (Exception e){

            return "There is something wrong, I cannot provide you that information at this moment";
        }

    }

    @RequestMapping(value = "gen/user/app", method = RequestMethod.POST)
    @ResponseBody
    public String generalAppUser(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass
    ) {

        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        if(us.isApp()) {
            return "Congratulations, your application has been already approved";
        } else {

            return "There are still documents that are missing or rejected";
        }
    }

    @RequestMapping(value = "/gen/userdoc/status", method = RequestMethod.POST)
    @ResponseBody
    public String generalUserDocStatus(
            @RequestParam("user") String user,
            @RequestParam("pass") String pass,
            @RequestParam("doc_type") String doc_type
    ) {
        User us = genAuthUser(user, pass);

        if (us == null) {
            return "Authentication failed, give me your password again please";
        }

        try {
            UserDocType document = getUserDocTypeByType(us.getName(), doc_type);

            if(document == null) {
                return "No document of that type has been submitted";
            } else {

                return "Your " + mapDocType(document.getUsertype().split("\\|")[1]) + " has been " + document.getUserstatus().split("\\|")[1] + " with the following feedback: " + document.getDetails();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Unable to find any document of that type for you " + us.getName();
    }

    public User genAuthUser(String name, String password)
    {
        User us = getUser(name);
        float passf = Float.parseFloat(password);
        int pass = Math.round(passf);

        if (us == null)
            return null;
        if(!us.getPass().equals(pass+""))
            return null;
        else
            return us;
    }

}
