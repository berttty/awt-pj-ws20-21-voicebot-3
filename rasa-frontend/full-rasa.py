#!/bin/python
import requests
import json
from modulefinder import IMPORT_NAME
import gtts
from playsound import playsound
import pyttsx3
import speech_recognition as sr

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def textToAudio(text):
    # make request to google to get synthesis
    tts = gtts.gTTS(text)

    # save the audio file
    tts.save('respond.mp3')
    playsound('respond.mp3')


def audioToText():
    # obtain audio from the microphone
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)

    # recognize speech using Google Speech Recognition
    try:
        text = r.recognize_google(audio)
        print(" ")
        print(" ")
        print("#####################################")
        print(text)
        print("#####################################")
        print(" ")
        print(" ")

    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
    return text

id = id_generator()
while(1):

    text = audioToText()
    payload = {"sender": id, "message": text}
    request = requests.post(
                        'http://localhost:5005/webhooks/rest/webhook',
                        json=payload,
                        headers = {"Content-Type": "application/json"}).json()
    for node in request:
        print(node['text'])
        textToAudio(node['text'])
