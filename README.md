# Voice Bot prototypes

The following instruction are designed to allow you to test and run all the steps.

## REQUIREMENTS

You need to have installed the following apps and dependecies in your laptop:

- python version 3.7 or superior
- Docker engine version 20.10.12 or superior
- Docker client version 20.10.12 or superior
- Kubernetes client version 1.22.5 or superior
- Kubernetes Server version 1.22.5 or superior
- Java version 1.8, not supported for superior versions
- git version 2.34.1 or superior

Create the alias in your `.*rc` file
```bash
echo 'export chatbot=$(pwd)' >> ~/.*rc
source ~/.*rc
```

> *NOTE:* replace ~/.*rc by your file terminal rc

> *NOTE:* to execute the previous command you need to be in the folder root of this project


## Instalation of MongoDB

for installing the mongoDB you need to follow the next steps:

```bash
cd ${chatbot}/mongo-db
chmod +x ./init.sh
./init.sh
```
> *NOTE:* the program required the port 27017 free

> *NOTE:* the deployment of mongo it will take around 5 minutes, just be patient


## Instalation of the Backend

for installing the backend you need to follow the next steps:
```bash
cd ${chatbot}/backend
chmod +x ./mvnw
./mvnw clean compile package install 
java -jar target/voicebot-backend-0.0.1-SNAPSHOT.jar
```

> *NOTE:* the program required the port 8080 free

## Instalation of the RASA

for installing the backend you need to follow the next steps:

```bash
cd ${chatbot}/rasa-backend
```

### Prepare the enviroment

Create the network in docker:
```bash
docker network create --driver=bridge --subnet=172.18.0.0/16 rasa-net
```

Create the alias in your `.*rc` file
```bash
echo 'alias rasa="docker run -v $(pwd):/app -p 5005:5005 --net rasa-net rasa/rasa:3.0.8-full"' >> ~/.*rc
echo 'alias rasa-action="docker run -v $(pwd):/app -p 5055:5055 --net rasa-net rasa/rasa:3.0.8-full"' >> ~/.*rc
source ~/.*rc
```

> *NOTE:* replace ~/.*rc by your file terminal rc

### Execute RASA backend

you need to train rasa in your local machine 
```bash
rasa train
```

after the train phase you need to run the backend
```bash
rasa run &
```
> *NOTE:* the program required the port 5005 free

after the train phase you need to run the backend actions
```bash
rasa-action run actions &
```
> *NOTE:* the program required the port 5055 free

### Execute RASA Frontend

Rasa have to mode of capture the input, either with audio or writing the instruction like chating with 
a human.

#### Execute RASA Frontend-Audio
you need to install the following package:

in the case of windows
```bash
pip3 install pipwin
pipwin install pyaudio
pip3 install gTTS pyttsx3 playsound speech_recognition
```

in the case of MacOS
```bash
brew install portaudio
pip3 install pyaudio speech_recognition gTTS pyttsx3 playsound
```

to run the code 
```bash
cd ${chatbot}/rasa-frontend
python3 full-rasa.py
```


#### Execute RASA Frontend-text
you need to install the following package:

```bash
pip3 install pyttsx3 playsound
```

to run the code
```bash
cd ${chatbot}/rasa-frontend
python3 full-text.py
```

## Instalation of the Webhook

This component will be use by DialogFlow and Amazon Lex

### REQUIMENTS
> *NOTE:* You need to have an SSL certificate to be able to run
> *NOTE:* You need to have a domain linked to the SSL

In addition to that you need to install the package 

```bash
pip install Flask
```

### Running the Webhook

To start the webhook you need to follow the below commands
```bash
cd ${chatbot}/webhook
export FLASK_APP=webhook.py 
python3 -m flask run --cert=${chatbot}/webhook/server.crt --key=${chatbot}/webhook/server.key --host=0.0.0.0
```


## Instalation DialogFlow prototype

> *NOTE:* You need to have an GCP account

To deploy the dialogflow you need to follow the instruction in the site [Prebuilt agents](https://cloud.google.com/dialogflow/cx/docs/concept/agents-prebuilt),
you will need to upload the following files:

```bash
cd ${chatbot}/dialogflow
ls -lah 
```

The current name of the file is `Dialogflow_agent_University applications Voice-bot.blob`

> *NOTE:* you need update the webhook url regarding the configuration in the step of webhook that you 
> did previously (SSL certificate is mandatory)

## Instalation Amazon Lex prototype

> *NOTE:* You need to have an AWS account 

The amazon is using two component the Lambda function and the Voicebot, both of them need to be
installed

#### Importing the Lambda Function

To deploy the lambda function succesfully you need to follow the site [Import a Lambda function as a component](https://docs.aws.amazon.com/greengrass/v2/developerguide/import-lambda-function-console.html)
you will need to upload the following files:

```bash
cd ${chatbot}/amazon-lex
ls -lah | grep 'voicebot-time-remaining'
```

The current name of the file is `voicebot-time-remaining-2fbc5fbf-3f74-42c4-aafe-d52c7cd2531c.zip`

> *NOTE:* you need update the webhook url regarding the configuration in the step of webhook that you
> did previously (SSL certificate is mandatory)


#### Importing the Lex Agent

To deploy the Lex Agent succesfully you need to follow the site [Importing and Exporting Amazon Lex Bots](https://docs.aws.amazon.com/lex/latest/dg/import-export.html)
you will need to upload the following files:

```bash
cd ${chatbot}/amazon-lex
ls -lah | grep 'AWS_UniversityApplicationsVoicebot'
```

The current name of the file is `AWS_UniversityApplicationsVoicebot_2_4c9acbfd-d546-420b-9f44-49d99ebdfc35_Bot_LEX_V1.zip`
