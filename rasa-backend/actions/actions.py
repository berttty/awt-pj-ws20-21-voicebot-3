# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
import logging
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
import requests
import json
from rasa_sdk.types import DomainDict

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
class ActionRetrieveDocument(Action):

    def name(self) -> Text:
        return "action_retrieve_document"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        logging.debug('A debug message4!')
        request = requests.get('http://host.docker.internal:8080/time/remaining').json()  # make an api call
        joke = request['timestamp']  # extract a joke from returned json response
        dispatcher.utter_message(text=joke)  # send the message back to the user
        dispatcher.utter_message(text=tracker.get_slot('name'))  # send the message back to the user
        dispatcher.utter_message(text=tracker.get_slot('password'))  # send the message back to the user

        dispatcher.utter_message(text="terrible de response")

        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
class ActionRetrieveAppStatus(Action):
    def name(self) -> Text:
        return "action_retrieve_app_status"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        payload = {"user": tracker.get_slot('name'), "pass": tracker.get_slot('password')}
        request = requests.post('http://host.docker.internal:8080/gen/user/app', data=payload)
        dispatcher.utter_message(text=request.text)  # send the message back to the user

        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################

class ActionRetrieveTimeRemaining(Action):

    def name(self) -> Text:
        return "action_retrieve_time_remaining"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        request = requests.post('http://host.docker.internal:8080/gen/time/remaining')  # make an api call
        time_remaining = request.content.decode('utf-8') # extract a joke from returned json response
        dispatcher.utter_message(text=time_remaining)  # send the message back to the user

        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################

class ActionRetrieveDocMissing(Action):

    def name(self) -> Text:
        return "action_retrieve_doc_missing"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        payload = {"user": tracker.get_slot('name'), "pass": tracker.get_slot('password')}
        request = requests.post('http://host.docker.internal:8080/gen/userdoc/missing', data=payload)
        dispatcher.utter_message(text=request.text)  # send the message back to the user
        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################

class ActionRetrieveDocSubmitted(Action):

    def name(self) -> Text:
        return "action_retrieve_doc_submitted"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        payload = {"user": tracker.get_slot('name'), "pass": tracker.get_slot('password')}
        request = requests.post('http://host.docker.internal:8080/gen/userdoc/submitted', data=payload)
        dispatcher.utter_message(text=request.text)  # send the message back to the user
        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
class ActionRetrieveDocProblems(Action):

    def name(self) -> Text:
        return "action_retrieve_doc_problems"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        payload = {"user": tracker.get_slot('name'), "pass": tracker.get_slot('password')}
        request = requests.post('http://host.docker.internal:8080/gen/userdoc/rejected', data=payload)
        dispatcher.utter_message(text=request.text)  # send the message back to the user
        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################
class ActionRetrieveDocStatus(Action):

    def name(self) -> Text:
        return "action_retrieve_doc_status"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        payload = {"user": tracker.get_slot('name'), "pass": tracker.get_slot('password'), "doc_type": tracker.get_slot('doc_type')}
        request = requests.post('http://host.docker.internal:8080/gen/userdoc/status', data=payload)
        dispatcher.utter_message(text=request.text)  # send the message back to the user
        return []

####################################################################################################
####################################################################################################
####################################################################################################
####################################################################################################

class ValidateUniversityForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_university_form"

    def validate_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate name value."""
        logging.debug('A debug message888!')
        return {"name": slot_value}